/*global define, $, console, window, history, document, tau*/

/**
 * Init page module
 */

define({
    name: 'views/page/init',
    requires: [
        'core/event',
        'core/template',
        'core/systeminfo',
        'core/application',
        'core/storage/idb',
        'views/page/main',
        'views/page/details',
        'views/page/units',
        'views/page/delete'
    ],
    def: function viewsPageInit(req) {
        'use strict';

        var e = req.core.event,
            idb = req.core.storage.idb,
            app = req.core.application,
            sysInfo = req.core.systeminfo;

        /**
         * Exits the application, waiting first for pending storage requests
         * to complete.
         */
        function exit() {
            e.fire('application.exit');
            if (!idb.hasPendingRequests()) {
                app.exit();
            } else {
                e.listen('core.storage.completed', app.exit);
            }
        }

        function onHardwareKeysTap(ev) {
            var keyName = ev.keyName,
                page = document.getElementsByClassName('ui-page-active')[0],
                pageid = page ? page.id : '';
            if (keyName === 'back') {
                if (pageid === 'main' || pageid === 'ajax-loader') {
                    exit();
                } else if (pageid === 'delete') {
                    tau.changePage('#details', {fromHashChange: true});
                } else if (pageid === 'settings') {
                    tau.changePage('#main');
                } else if (pageid === 'register') {
                    e.fire('views.register.menuBack');
                    history.back();
                } else {
                    history.back();
                }
            }
        }

        /**
         * Handler onLowBattery state
         */
        function onLowBattery() {
            exit();
        }

        function bindEvents() {
            window.addEventListener('tizenhwkey', onHardwareKeysTap);
            sysInfo.listenBatteryLowState();
        }

        function init() {
            // bind events to page elements
            bindEvents();
            sysInfo.checkBatteryLowState();
        }

        e.listeners({
            'core.battery.low': onLowBattery
        });

        return {
            init: init
        };
    }

});
