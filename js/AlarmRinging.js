var canvas;
var context;
var clockRadius;
var alarmStorage;

var exit;
var on;
var time;
var yoil = [];
var mode;
var off_mode;
var sound;
var vibration;
var volume;
var repeat;
var snooz;

var tempTime = "-1:-1";
var tempMode;
var tempOff_mode;
var tempSound;
var tempVibration;
var tempVolume;
var tempRepeat;
var tempSnooze;

var count1Minute;

window.onload = function() {
    canvas = document.querySelector("canvas");
    context = canvas.getContext("2d");

    //Assigns the area that will use Canvas
//    canvas.width = document.width;
//      canvas.height = canvas.width;
    
    canvas.width = document.width;
    canvas.height = 200;
    
  //alarmStorage = windows.localStorage.getLength();
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
	
	var tDate = date.getDate();		//일자 반환(from 1-31)
	var days = date.getDay();		//요일 반환(from 0-6)
	var months = date.getMonth();	//월 반환(from 0-11)
	var year = date.getFullYear();	//4자리 년도 반환(for digits)
	
	
	  //끝에 _0~3이 붇는다! - 1알람당 16개
	window.localStorage.setItem( 'exit_0', "1"); //0 : off
	window.localStorage.setItem( 'on_0', "1"); //0 : off
	window.localStorage.setItem( 'time_0', date.getHours() + ":" + date.getMinutes());	//24hour
   window.localStorage.setItem( 'yoil_0', "1");  //sun 
   window.localStorage.setItem( 'yoil_1', "0");  //mon
   window.localStorage.setItem( 'yoil_2', "1"); 
   window.localStorage.setItem( 'yoil_3', "0"); 
   window.localStorage.setItem( 'yoil_4', "0"); 
   window.localStorage.setItem( 'yoil_5', "1"); 
   window.localStorage.setItem( 'yoil_6', "1"); 
   window.localStorage.setItem( 'mode', "t");  //t:touch, h:heartbeat, p:pedometer, g:game"
   window.localStorage.setItem( 'off_mode', "0"); //h:심박수, p:걸음수
   window.localStorage.setItem( 'sound', "0"); //1 : on
   window.localStorage.setItem( 'vibration', "1"); //1 : on
   window.localStorage.setItem( 'volume', "3");  //0~4
   window.localStorage.setItem( 'repeat', "3");  //0~4. 1,2,3,5,10
   window.localStorage.setItem( 'snooz', "0");   //0~4  2,3,5,10,15
   
   
   var temOnTime = tempTime.split(":", 2);
   if ((((temOnTime[0] *= 1) == date.getHours()) && (( temOnTime[1] *= 1) == minutes))) {
	   	//console.log("fcRingingSubmit()");
	   	fcRingingSubmit();
	   	count1Minute = minutes;
   }
   

	console.log(localStorage.length);
    for (i=0; i<localStorage.length; i++) {
    	var keyValue = localStorage.key(i);
    	console.log(keyValue);
    	if (keyValue.match("on_") != null) {
    		
    		if (time != null) {
    			var onTime = time.split(":",2);
	    	    if ((onTime[0] *= 1) == date.getHours() && ( onTime[1] *= 1) == minutes && yoil[days]) {
	    	    	console.log("Alarm Ringing!");
	    	    	fcRingingSubmit();
	    	    	count1Minute = minutes;
	    	    	break;
	    	    }   
    		}
	    	    
    		if (localStorage.getItem(keyValue) == "0") {
    			i += 15;
    			console.log(i);
    		}
    	}
    	if(keyValue.match("time_") != null) 	time = localStorage.getItem(keyValue);
    	if(keyValue.match("yoil_0") != null) 	yoil[0] = localStorage.getItem(keyValue);
    	if(keyValue.match("yoil_1") != null) 	yoil[1] = localStorage.getItem(keyValue);
    	if(keyValue.match("yoil_2") != null) 	yoil[2] = localStorage.getItem(keyValue);
    	if(keyValue.match("yoil_3") != null) 	yoil[3] = localStorage.getItem(keyValue);
    	if(keyValue.match("yoil_4") != null) 	yoil[4] = localStorage.getItem(keyValue);
    	if(keyValue.match("yoil_5") != null) 	yoil[5] = localStorage.getItem(keyValue);
    	if(keyValue.match("yoil_6") != null) 	yoil[6] = localStorage.getItem(keyValue);
    	if(keyValue.match("mode") != null) 		mode = localStorage.getItem(keyValue);
    	if(keyValue.match("off_mode") != null) 	off_mode = localStorage.getItem(keyValue);
    	if(keyValue.match("sound") != null) 	sound = localStorage.getItem(keyValue);
    	if(keyValue.match("vibration") != null) 	vibration = localStorage.getItem(keyValue);
    	if(keyValue.match("volume") != null) 	volume = localStorage.getItem(keyValue);
    	if(keyValue.match("repeat") != null) 	tempRepeat = repeat = localStorage.getItem(keyValue);
    	if(keyValue.match("snooz") != null) 	snooz = localStorage.getItem(keyValue);
    }
    
    if (localStorage.getItem('tempRepeat') != null)	tempRepeat = localStorage.getItem('tempRepeat');
    
    if (time != null) {
		var onTime = time.split(":",2);
		console.log(onTime[0]+" "+ date.getHours()+ " " + onTime[1] + " " + minutes);
	    if ((onTime[0] *= 1) == date.getHours() && ( onTime[1] *= 1) == minutes && yoil[days]) {
	    	//console.log("Alarm Ringing!");
	    	fcRingingSubmit();
	    	count1Minute = minutes;
	    }   
	}
  
       

    /*
    //add eventListener for tizenhwkey
    window.addEventListener('tizenhwkey', function(e) {
        if(e.keyName == "back")
            tizen.application.getCurrentApplication().exit();
    });
*/
    setInterval(watch, 500);
	window.requestAnimationFrame(watch);
}

function fcRingingSubmit() {
	
	//알람페이지가 안보이면 알람페이지를 띄움
	if (document.getElementById("alarmRinging").style.display == "none") {
		kuisin("snoozeMode");
		kuisin("alarmRinging");
  	}
  	
  	if (vibration == "1")
   		singleVibration();
   	else
   		stopVibration();
   		
	if (sound == "1")
  		startAlarmRinging();
   	else
   		stopAlarmRinging();
}

function fcSnoozeSubmit() {

	//snoozeMode로 ㄱㄱ
	if (document.getElementById("snoozeMode").style.display == "none") {
		
		tempRepeat--;
		console.log("repeat : " + tempRepeat);
		if (tempRepeat > 0) {
			
			// Gets the current application id.
			 var appId = tizen.application.getCurrentApplication().appInfo.id; 
			
			 //0~4  2,3,5,10,15
			 var snoozeInterval;
			 if (snooz == 0)	snoozeInterval = 1 * tizen.alarm.PERIOD_MINUTE;
			 else if (snooz == 1)	snoozeInterval = 3 * tizen.alarm.PERIOD_MINUTE;
			 else if (snooz == 2)	snoozeInterval = 5 * tizen.alarm.PERIOD_MINUTE;
			 else if (snooz == 3)	snoozeInterval = 10 * tizen.alarm.PERIOD_MINUTE;
			 else if (snooz == 4)	snoozeInterval = 15 * tizen.alarm.PERIOD_MINUTE;
			 
			 var tempSnoozeIn;
			 if (snooz == 0)	tempSnoozeIn = 1;
			 else if (snooz == 1)	tempSnoozeIn = 3;
			 else if (snooz == 2)	tempSnoozeIn = 5;
			 else if (snooz == 3)	tempSnoozeIn = 10;
			 else if (snooz == 4)	tempSnoozeIn = 15;
			 
			 var date = new Date();
			 var tempHour = date.getHours();
			 var tempMin = date.getMinutes() + tempSnoozeIn;
			 if (tempMin > 60) {
			 	tempHour = (tempHour + 1) % 24;
			 	tempMin = tempMin % 60;
			 }
			 
			 window.localStorage.setItem( 'tempTime', tempHour + ":" + tempMin);	//24hour
			window.localStorage.setItem( 'tempMode', mode);  //t:touch, h:heartbeat, p:pedometer, g:game"
			window.localStorage.setItem( 'tempOff_mode', off_mode); //h:심박수, p:걸음수
			window.localStorage.setItem( 'tempSound', sound); //1 : on
			window.localStorage.setItem( 'tempVibration', vibration); //1 : on
			window.localStorage.setItem( 'tempVolume', volume);  //0~4
			window.localStorage.setItem( 'tempRepeat', tempRepeat);  //0~4. 1,2,3,5,10
			window.localStorage.setItem( 'tempSnooze', snooz);   //0~4  2,3,5,10,15
			 
			 var alarm = new tizen.AlarmRelative(snoozeInterval);
			 tizen.alarm.add(alarm, appId);
			 
			 var sec = alarm.getRemainingSeconds();
			 console.log("remaining time is " + sec);
			 
			 document.getElementById("main").style.backgroundImage = "url(../image_zip/SnoozeMode/SnoozeMode_background.png)";
			kuisin("snoozeMode");
			kuisin("alarmRinging");
		 }
		 else {
		 	alarmExit();
		 }
				
	}
	
	stopVibration();
	stopAlarmRinging();
	
}




( function () {
	window.addEventListener( 'tizenhwkey', function( ev ) {
		if( ev.keyName == "back" ) {
			var page = document.getElementsByClassName( 'ui-page-active' )[0],
				pageid = page ? page.id : "";
			if( pageid === "main" ) {
				tizen.application.getCurrentApplication().exit();
			} else {
				window.history.back();
			}
		}
	} );
} () );


function singleVibration()
{
	navigator.vibrate([1000, 1000, 2000, 2000, 2000, 2000,
	                   1000, 1000, 2000, 2000, 2000, 2000,
	                   1000, 1000, 2000, 2000, 2000, 2000,
	                   1000, 1000, 2000, 2000, 2000, 2000,
	                   1000, 1000, 2000, 2000, 2000, 2000,
	                   1000, 1000, 2000, 2000, 2000, 2000]);
}

function stopVibration()
{
   navigator.vibrate(0);
} 

function watch() {
    //Erase the previous time
	context.clearRect(0, 0, context.canvas.width, 200);

    //Import the current time
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
	
	var tDate = date.getDate();		//일자 반환(from 1-31)
	var days = date.getDay();		//요일 반환(from 0-6)
	var months = date.getMonth();	//월 반환(from 0-11)
	var year = date.getFullYear();	//4자리 년도 반환(for digits)
	
	
	var ampm = hours > 12? "PM" : "AM";
    hours = hours > 12 ? hours - 12 : hours;
    
    hours = hours < 10? "0"+hours : hours;
    hours = hours == "00"? 12 : hours;
    minutes = minutes < 10? "0"+minutes : minutes;
    context.save();
    
    
    //Assign the style of the number which will be applied to the clock plate
    context.beginPath();
    
    context.textAlign = "center";
    context.textBaseline = "middle";
    context.fillStyle = "#fff";
    
    
    
    var month;
    switch (months) {
    	case 0 : month = "January";	break;
    	case 1 : month = "February";	break;
    	case 2 : month = "March";	break;
    	case 3 : month = "April";	break;
    	case 4 : month = "May";	break;
    	case 5 : month = "June";		break;
    	case 6 : month = "July";	break;
    	case 7 : month = "August";	break;
    	case 8 : month = "September";	break;
    	case 9 : month = "October";	break;
    	case 10 : month = "November";	break;
    	case 11 : month = "December";	break;
    }
    
    var day;
    switch (days) {
    	case 0 : day = "Sunday";	break;
    	case 1 : day = "Monday";	break;
    	case 2 : day = "Tuesday";	break;
    	case 3 : day = "Wednesday";	break;
    	case 4 : day = "Thursday";	break;
    	case 5: day = "Friday";		break;
    	case 6 : day = "Saturday";	break;
    }
    
    var snozDiv = document.getElementById("snoozeMode");
    var ringDiv = document.getElementById("alarmRinging");
    
//    var onTime = time.split(":",2);
//    var temOnTime = tempTime.split(":", 2);
//    if (repeatVal != minutes && ((onTime[0] *= 1) == date.getHours() && ( onTime[1] *= 1) == minutes && yoil[days]) || 
//    		(((temOnTime[0] *= 1) == date.getHours()) && (( temOnTime[1] *= 1) == minutes))) {
//    	console.log("fcRingingSubmit()");
//    	fcRingingSubmit();
//    	repeatVal = count1Minute = minutes;
//    }
    
    if(ringDiv.style.display=="block") { 
    	if (Math.abs(count1Minute-minutes) == 1) {
    		count1Minute = null;
    		console.log("fcSnoozeSubmit");
    		//ringDiv.style.display="none";
    		fcSnoozeSubmit();
    	}
    	
    	context.font = "30px monospace";
        //context.fillText(ampm, canvas.width / 1.2, canvas.height / 2.4);
        context.fillText(ampm, canvas.width / 1.2, 135);

        context.font = "82px monospace";
        //context.fillText(hours + ":" + minutes, canvas.width / 2.5, canvas.height / 2.8);
        context.fillText(hours + ":" + minutes, canvas.width / 2.5, 120);
        
        context.font = "30px bold monospace";
        //context.fillText(day+", "+month+" "+tDate, canvas.width / 2, canvas.height / 1.8);
        context.fillText(day+", "+month+" "+tDate, canvas.width / 2, 180);
    }
    else{ 
    	
    	context.font = "25px monospace";
        //context.fillText(ampm, canvas.width / 1.2, canvas.height / 2.4);
        context.fillText(ampm, canvas.width / 1.3, 75);

        context.font = "72px monospace";
        //context.fillText(hours + ":" + minutes, canvas.width / 2.5, canvas.height / 2.8);
        context.fillText(hours + ":" + minutes, canvas.width / 2.4, 60);
        
        context.font = "25px bold monospace";
        //context.fillText(day+", "+month+" "+tDate, canvas.width / 2, canvas.height / 1.8);
        context.fillText(day+", "+month+" "+tDate, canvas.width / 2, 120);
        
    }
    
    
    context.restore();
	window.requestAnimationFrame(watch);
}



var currentFile = "";
function playAudio() {
    // Check for audio element support.
    if (window.HTMLAudioElement) {
        try {
            var oAudio = document.getElementById('myaudio');
            var btn = document.getElementById('play'); 
            var audioURL = document.getElementById('audiofile'); 

            //Skip loading if current file hasn't changed.
            if (audioURL.value !== currentFile) {
                oAudio.src = audioURL.value;
                currentFile = audioURL.value;                       
            }
        }
        catch (e) {
            // Fail silently but show in F12 developer tools console
             if(window.console && console.error("Error:" + e));
        }
    }
}
