var canvas;
var context;
var clockRadius;

window.onload = function() {
    canvas = document.querySelector("canvas");
    context = canvas.getContext("2d");

    //Assigns the area that will use Canvas
//    canvas.width = document.width;
//      canvas.height = canvas.width;
    
    canvas.width = document.width;
    canvas.height = 200;

    /*
    //add eventListener for tizenhwkey
    window.addEventListener('tizenhwkey', function(e) {
        if(e.keyName == "back")
            tizen.application.getCurrentApplication().exit();
    });
*/
    setInterval(watch, 500);
	window.requestAnimationFrame(watch);
}




( function () {
	window.addEventListener( 'tizenhwkey', function( ev ) {
		if( ev.keyName == "back" ) {
			var page = document.getElementsByClassName( 'ui-page-active' )[0],
				pageid = page ? page.id : "";
			if( pageid === "main" ) {
				tizen.application.getCurrentApplication().exit();
			} else {
				window.history.back();
			}
		}
	} );
} () );



function watch() {
    //Erase the previous time
	context.clearRect(0, 0, context.canvas.width, 200);

    //Import the current time
    var date = new Date();
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var seconds = date.getSeconds();
	
	var tDate = date.getDate();		//일자 반환(from 1-31)
	var days = date.getDay();		//요일 반환(from 0-6)
	var months = date.getMonth();	//월 반환(from 0-11)
	var year = date.getFullYear();	//4자리 년도 반환(for digits)
	
	
	var ampm = hours > 12? "PM" : "AM";
    hours = hours > 12 ? hours - 12 : hours;
    
    hours = hours < 10? "0"+hours : hours;
    hours = hours == "00"? 12 : hours;
    minutes = minutes < 10? "0"+minutes : minutes;
    context.save();


    //Assign the style of the number which will be applied to the clock plate
    context.beginPath();
    
    context.textAlign = "center";
    context.textBaseline = "middle";
    context.fillStyle = "#fff";
    
    context.font = "25px monospace";
    //context.fillText(ampm, canvas.width / 1.2, canvas.height / 2.4);
    context.fillText(ampm, canvas.width / 1.3, 75);

    context.font = "72px monospace";
    //context.fillText(hours + ":" + minutes, canvas.width / 2.5, canvas.height / 2.8);
    context.fillText(hours + ":" + minutes, canvas.width / 2.4, 60);
    
    var month;
    switch (months) {
    	case 0 : month = "January";	break;
    	case 1 : month = "February";	break;
    	case 2 : month = "March";	break;
    	case 3 : month = "April";	break;
    	case 4 : month = "May";	break;
    	case 5 : month = "June";		break;
    	case 6 : month = "July";	break;
    	case 7 : month = "August";	break;
    	case 8 : month = "September";	break;
    	case 9 : month = "October";	break;
    	case 10 : month = "November";	break;
    	case 11 : month = "December";	break;
    }
    
    var day;
    switch (days) {
    	case 0 : day = "Sunday";	break;
    	case 1 : day = "Monday";	break;
    	case 2 : day = "Tuesday";	break;
    	case 3 : day = "Wednesday";	break;
    	case 4 : day = "Thursday";	break;
    	case 5: day = "Friday";		break;
    	case 6 : day = "Saturday";	break;
    }
    
    context.font = "25px bold monospace";
    //context.fillText(day+", "+month+" "+tDate, canvas.width / 2, canvas.height / 1.8);
    context.fillText(day+", "+month+" "+tDate, canvas.width / 2, 120);
    
    context.restore();
	window.requestAnimationFrame(watch);
}



//Get 파라메터값 받아오기
var getParam = function(key){
    var _parammap = {};
    document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
        function decode(s) {
            return decodeURIComponent(s.split("+").join(" "));
        }

        _parammap[decode(arguments[1])] = decode(arguments[2]);
    });

    return _parammap[key];
};

//파라메터 값 받아와서 알람 종료시 tizen.alarm.remove(id값);해준다.
var temp = getParam("name");
var ringing = getParam("managerId");

